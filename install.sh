#!/usr/bin/env bash

# Adrien Luxey - Feb. 2019
# Script inspired by https://github.com/mathiasbynens/dotfiles/blob/master/bootstrap.sh

# Get dotfiles dir
DOTDIR=$(cd "$(dirname ${BASH_SOURCE})"; pwd)
PRIVATE_DOTFILES_DIR="${DOTDIR}/dotfiles_private/"
INSTALLED_FLAG_FILE="${DOTDIR}/.installed"
SUBLIME=0
INSTALL=0

function main {
	parseArgs $@
	# Do the things
	if [[ ${INSTALL} -eq 1 ]]; then
		if [ ! -f ${INSTALLED_FLAG_FILE} ]; then
			echo "Stuff seems already installed. Remove \`.installed\` if this is false."
		else
			installDependencies && touch ${INSTALLED_FLAG_FILE}
		fi
	fi
	setupDotfiles
	echo "All done!"
	exit 0
}

function printHelp {
	cat <<-EOF
	USAGE: $0 [OPTIONS]

	OPTIONS:
		-i|--install	Install software for Debian (incl. Tmux & vim but not Sublime Text)
		-s|--sublime 	Install & Configure Sublime Text 3 for Debian
		-h|--help 		Show this help

	Sets up dotfiles, 
	The script will install dependencies once (remove ${INSTALLED_FLAG_FILE} to redo) and symlink dotfiles to their destination in your \${HOME}.
	EOF
	exit $1;
}

function parseArgs {
	while (( "$#" )); do 
		case "$1" in 
			-h|--help)
				printHelp 0
				shift
				;;
			-s|--sublime)
				SUBLIME=1
				shift
				;;
			-i|--install)
				INSTALL=1
				shift
				;;
			--)
				shift
				break 
				;;
			-*|--*=)
				echo "Error: Unsupported flag $1" >&2
				printHelp 1
				;;
		esac
	done
}

function setupDotfiles {
	read -p "Will now remove previous dotfiles. Ready?" -n 1 -r

	rm ${HOME}/.zshrc > /dev/null 2>&1
	rm ${HOME}/.vimrc > /dev/null 2>&1
	rm ${HOME}/.tmux.conf > /dev/null 2>&1
	rm -rf ${HOME}/.vim > /dev/null 2>&1
	rm -rf ${HOME}/.tmux > /dev/null 2>&1
	rm ${HOME}/.ssh/config > /dev/null 2>&1

	echo Setting up dotfiles...
	ln -s ${DOTDIR}/zshrc ${HOME}/.zshrc
	ln -s ${DOTDIR}/vimrc ${HOME}/.vimrc
	ln -s ${DOTDIR}/tmux.conf ${HOME}/.tmux.conf
	ln -s ${DOTDIR}/vim ${HOME}/.vim
	ln -s ${DOTDIR}/tmux ${HOME}/.tmux
	mkdir -p ${HOME}/.ssh
	ln -s ${DOTDIR}/dotfiles_private/ssh_config ~/.ssh/config

	# Setup Sublime Text
	if [[ ${SUBLIME} -eq 1 ]]; then
		echo 
		if [ -d "${HOME}/.config/sublime-text-3" ]; then
			rm -rf ${HOME}/.config/sublime-text-3/Packages/User > /dev/null 2>&1
			ln -s ${DOTDIR}/sublime ${HOME}/.config/sublime-text-3/Packages/User 	
		elif [ -d "${HOME}/.config/sublime-text" ]; then
			rm -rf ${HOME}/.config/sublime-text/Packages/User > /dev/null 2>&1
			ln -s ${DOTDIR}/sublime ${HOME}/.config/sublime-text/Packages/User 	
		fi
	fi
}

function installDependencies {
	read -p "Will now install apt dependencies, OhMyZSH (wget | sh) and sub-repos. Ready?" -n 1 -r

	# Dependencies
	sudo apt update  > /dev/null 2>&1
	sudo apt install -y apt-transport-https curl zsh tmux autojump \
		fonts-firacode fonts-powerline fzf vim > /dev/null 2>&1

	# Install Sublime Text
	if [[ ${SUBLIME} -eq 1 ]]; then
		echo Installing Sublime Text 3...
		wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | \
			gpg --dearmor | \
			sudo tee /etc/apt/trusted.gpg.d/sublimehq-archive.gpg > /dev/null
		echo "deb https://download.sublimetext.com/ apt/stable/" | \
			sudo tee /etc/apt/sources.list.d/sublime-text.list > /dev/null 2>&1
		sudo apt update > /dev/null 2>&1
		sudo apt install sublime-text -y > /dev/null 2>&1
	fi

	# Install Oh My Zsh
	if [ ! -d "${HOME}/.oh-my-zsh" ]; then
		echo Installing Oh-My-Zsh...
		sh -c "$(wget -q https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh -O -)" 
		# Install powerlevel9k theme in Oh-My-Zsh
		if [ ! -d "${HOME}/.oh-my-zsh/custom/themes/powerlevel9k" ]; then
			echo Installing Powerlevel9k theme...
			# git clone https://github.com/bhilburn/powerlevel9k.git \
			# 	${HOME}/.oh-my-zsh/custom/themes/powerlevel9k > /dev/null 2>&1
			git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/themes/powerlevel10k
		fi
	fi

	read -p "Adding adrien/dotfiles_private submodule... Ready?" -n 1 -r
	git submodule add --name prive git@git.deuxfleurs.fr:adrien/dotfiles_private.git "${PRIVATE_DOTFILES_DIR}"


	echo Pulling git submodules...
	cd ${DOTDIR} && (git submodule update --init >> /dev/null 2>&1) || \
		echo "'git submobule update --init' failed, is your SSH key available on GitHub?" && exit -1;
	
	# Make ZSH is the current SHELL
	echo $SHELL | grep zsh > /dev/null || chsh -s $(which zsh)
}

main $@
