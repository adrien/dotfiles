"""""""""""""""""
" Vundle config "
"""""""""""""""""

set nocompatible              " be iMproved, required
filetype off                  " required

set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

Plugin 'christoomey/vim-tmux-navigator' " Vim Tmux Navigator 

Plugin 'vim-airline/vim-airline' " Airline bar
Plugin 'vim-airline/vim-airline-themes' " Airline themes

Plugin 'LnL7/vim-nix' " Nix

" Themes
" Plugin 'arcticicestudio/nord-vim'

" All of your Plugins must be added before the following line
call vundle#end()            " required
filetype plugin indent on    " required

"""""""""""""""""""""
" End Vundle config "
"""""""""""""""""""""

""""""""""""""
" Navigation "
""""""""""""""

" Jump beginning/end of file: gg/G
" Jump to line 30: 30G
" Jump 50% up/down: Ctrl-d/Ctrl-u
" Jump prev/next word: b/w

" From https://stackoverflow.com/questions/33245317/vim-and-azerty-keyboards
" 1. Move to the end of the line
noremap m $
" 2. Search backward for the word under the cursor
noremap µ #
" 3. Navigate to the help tag under the cursor
" noremap ' <C-]>

" Scroller comme un pro
" N'utilise que j et k
" Ctrl-e & Ctrl-y pour scroller dans un document
set scrolloff=12

" Ctrl+W +/-: increase/decrease height (ex. 20<C-w>+)
" Ctrl+W >/<: increase/decrease width (ex. 30<C-w><)
" Ctrl+W =: equalize width and height of all windows
" Ctrl+W _: set height (ex. 50<C-w>_)
" Ctrl+W |: set width (ex. 50<C-w>|)

""""""""""""""""""""
" Tmux integration "
""""""""""""""""""""

" auto-reload tmux conf when updated
if has("autocmd")
    autocmd bufwritepost ~/dotfiles/.tmux.conf execute ':!tmux source-file %'
endif
"""""""""""
" Theming "
"""""""""""

syntax enable
set background=dark
colorscheme blues
"let g:airline_theme='minimalist' " The airline theme
let g:airline_powerline_fonts=1 " Make airline use powerline fonts (pretty arrows)

""""""""
" Misc "
""""""""

" Autoreload vimrc
if has("autocmd")
    autocmd! bufwritepost .vimrc source ~/.vimrc
    autocmd! bufwritepost vimrc source ~/.vimrc
endif

" Allow saving of files as sudo when I forgot to start vim using sudo.
cmap w!! w !sudo tee > /dev/null %

" Map CAPSLOCK to Escape in insert mode?

" Wrap move left/right to end of last line/beginning of the next one
set whichwrap+=<,>,h,l,[,]

" Do not create swap files
set nobackup
set noswapfile
set noundofile

" Line numbering (hybrid: relative and absolute)
set number relativenumber
augroup numbertoggle autocmd!
  autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
  autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup END

" jk to <ESC>
inoremap jk <esc>

" Yank vim text to system & selection clipboard with Ctrl+C
" Paste with Ctrl+V
" Requires `gvim` that is the following flags to your `vim --version`
" +clipboard
" +X11
" +xterm_clipboard
" From https://www.youtube.com/watch?v=E_rbfQqrm7g
vnoremap <C-c> "*y :let @+=@*<CR>
map <C-v> "+P

" Toggle search high-lighting
nnoremap <F3> :set hlsearch!<CR>

" You wanna suffer? Disable arrow keys
" noremap <Up> <NOP>
" noremap <Down> <NOP>
" noremap <Left> <NOP>
" noremap <Right> <NOP>


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Adapted from https://github.com/jessfraz/.vim/blob/master/vimrc "
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" ----------------------------------------- "
" File Type settings                        "
" ----------------------------------------- "

au BufNewFile,BufRead *.vim setlocal noet ts=4 sw=4 sts=4
au BufNewFile,BufRead *.md setlocal nospell noet ts=4 sw=4
au BufNewFile,BufRead *.yml,*.yaml setlocal expandtab ts=2 sw=2
au BufNewFile,BufRead *.cpp setlocal expandtab ts=2 sw=2
au BufNewFile,BufRead *.hpp setlocal expandtab ts=2 sw=2
au BufNewFile,BufRead *.json setlocal expandtab ts=2 sw=2
au BufNewFile,BufRead *.jade setlocal expandtab ts=2 sw=2

augroup filetypedetect
  au BufNewFile,BufRead .tmux.conf*,tmux.conf* setf tmux
  au BufNewFile,BufRead .nginx.conf*,nginx.conf* setf nginx
augroup END

au FileType nginx setlocal noet ts=4 sw=4 sts=4

" Go settings
au BufNewFile,BufRead *.go setlocal noet ts=4 sw=4 sts=4
" autocmd BufEnter *.go colorscheme nofrils-dark

" scala settings
autocmd BufNewFile,BufReadPost *.scala setl shiftwidth=2 expandtab

" Markdown Settings
au BufNewFile,BufRead *.txt setlocal noet ts=4 sw=4
autocmd BufNewFile,BufReadPost *.md setl ts=4 sw=4 sts=4 expandtab

" lua settings
autocmd BufNewFile,BufRead *.lua setlocal noet ts=4 sw=4 sts=4

" Dockerfile settings
autocmd FileType dockerfile set noexpandtab

" shell/config/systemd settings
autocmd FileType fstab,systemd set noexpandtab
autocmd FileType gitconfig,sh,toml set noexpandtab

" python indent
autocmd BufNewFile,BufRead *.py setlocal tabstop=4 softtabstop=4 shiftwidth=4 textwidth=80 smarttab expandtab

"""""""""""""""""""

set splitright                  " Split vertical windows right to the current windows
set splitbelow                  " Split horizontal windows below to the current windows
set encoding=utf-8              " Set default encoding to UTF-8
set autowrite                   " Automatically save before :next, :make etc.
set autoread                    " Automatically reread changed files without asking me anything
set fileformats=unix,dos,mac    " Prefer Unix over Windows over OS 9 formats
set noshowmatch                 " Do not show matching brackets by flickering
set noshowmode                  " We show the mode with airline or lightline
set incsearch                   " Shows the match while typing
set hlsearch                    " Highlight found searches
set ignorecase                  " Search case insensitive...
set smartcase                   " ... but not when search pattern contains upper case characters

" Indent
set autoindent
set complete-=i
set showmatch
set smarttab

set et
set tabstop=4
set shiftwidth=4
set expandtab

set nrformats-=octal
set shiftround

" In many terminal emulators the mouse works just fine, thus enable it.
if has('mouse')
  set mouse=a
endif
" If linux then set ttymouse
let s:uname = system("echo -n \"$(uname)\"")
if !v:shell_error && s:uname == "Linux" && !has('nvim')
  set ttymouse=xterm
endif

" syntax enable
" if has('gui_running')
"   set transparency=3
"   " fix js regex syntax
"   set regexpengine=1
"   syntax enable
" endif
" set background=dark
" colorscheme solarized
" let g:solarized_termtrans=1
" let g:hybrid_use_Xresources = 1
" let g:rehash256 = 1
" set guifont=Inconsolata:h15
" set guioptions-=L

" Search mappings: These will make it so that going to the next one in a
" search will center on the line it's found in.
nnoremap n nzzzv
nnoremap N Nzzzv

" Do not show stupid q: window
map q: :q

" never do this again --> :set paste <ctrl-v> :set no paste
let &t_SI .= "\<Esc>[?2004h"
let &t_EI .= "\<Esc>[?2004l"

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" End https://github.com/jessfraz/.vim/blob/master/vimrc "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

