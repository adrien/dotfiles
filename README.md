# Adrien Luxey's dotfiles 

ZSH, Tmux, Vim and Sublime Text dotfiles for Debian-based distros (`install.sh` using `apt`).

## Usage

First download submodules:

* Push your new host's SSH key to your github.com account.
* `git submodule update --init`

Use `install.sh` to install the dependencies, Oh-My-Zsh, pull tmux and vim plugins, **remove your previous dotfiles** and symlink the new ones:

    $ ./install.sh -h
        USAGE: ./install.sh [OPTIONS]
    
        OPTIONS:
        -s|--sublime    Install Sublime Text 3
        -h|--help               Show this help
    
        The script will install dependencies once (remove $HOME/.installed to redo) and symlink dotfiles to their destination in your $HOME.
    
If you want to do your own thing from these dotfiles, go read `install.sh`.

## Some docs & refs

* [How to nest tmux sessions like a boss](https://www.freecodecamp.org/news/tmux-in-practice-local-and-nested-remote-tmux-sessions-4f7ba5db8795/)

## Licence

Public Domain, i.e. do whatever you want with this, but don't come and complain.

